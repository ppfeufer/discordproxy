# Operations Guide

## Installation

### Alliance Auth installation

This section describes how to install Discord Proxy into an existing Alliance Auth installation.

#### Bare metal

```{eval-rst}
.. note::
    This guide assumed a default installation according to the official Auth installation guide.
```

Login as root user, activate your venv and navigate to your Auth main folder:

```bash
cd /home/allianceserver/myauth
```

Install discordproxy from PyPI into the venv:

```bash
pip install discordproxy
```

Add Discord Proxy to your supervisor configuration for Auth.

Edit supervisor.conf in your current folder and add the below section. Make sure to replace `YOUR-BOT-TOKEN` with your current Discord bot token:

```ini
[program:discordproxy]
command=/home/allianceserver/venv/auth/bin/discordproxyserver --token "YOUR-BOT-TOKEN"
directory=/home/allianceserver/myauth/log
user=allianceserver
numprocs=1
autostart=true
autorestart=true
stopwaitsecs=120
stdout_logfile=/home/allianceserver/myauth/log/discordproxyserver.out
stderr_logfile=/home/allianceserver/myauth/log/discordproxyserver.err
```

```{eval-rst}
.. note::
    We do not recommend adding discordproxy to your myauth group, since it does not require to be restarted after myauth configuration changes like the other programs in that group.
```

Reload supervisor to activate the changes and start Discord Proxy:

```bash
supervisorctl reload
```

To verify Discord Proxy is up and running you can check it's status:

```bash
supervisorctl status discordproxy
```

It should say "RUNNING".

To verify your installation was successful we recommend to [test your server](#test-discord-proxy-server).

#### Docker AA4

```{eval-rst}
.. note::
    This section is a draft. We welcome any feedback on how to complete it.
```

The new way of running discordproxy in AA 4.x is with a small container build via macros:

```text
allianceauth_discordproxy:
container_name: allianceauth_discordproxy
<<: [*allianceauth-base]
entrypoint: [
    "/home/allianceauth/.local/bin/discordproxyserver",
    "--host",
    "0.0.0.0",
    "--token",
    "${DISCORD_BOT_TOKEN}"
]
```

### Stand-alone installation

This section describes how to install Discord Proxy as standalone server.

#### Create a Discord bot account

Follow [this guide](https://discordpy.readthedocs.io/en/latest/discord.html) to create your Discord bot:

1. Create a Discord application for your bot
2. Invite your bot to your Discord server

#### Install discordproxy on your server

Create a service user and switch to that user:

```bash
sudo adduser --disabled-login discordproxy
sudo su discordproxy
```

Setup a virtual environment for the server, activate it and update key packages:

```bash
cd /home/discordproxy
python3 -m venv venv
source venv/bin/activate
```

Update and install basic packages:

```bash
pip install -U pip
pip install wheel setuptools
```

Install discordproxy from PyPI into the venv:

```bash
pip install discordproxy
```

#### Add discordproxy to supervisor

Create a supervisor configuration file - `/home/discordproxy/discordproxyserver.conf` - with the below template:

```ini
[program:discordproxy]
command=/home/discordproxy/venv/bin/discordproxyserver --token "YOUR-BOT-TOKEN"
directory=/home/discordproxy
user=discordproxy
numprocs=1
autostart=true
autorestart=true
stopwaitsecs=120
stdout_logfile=/home/discordproxy/discordproxyserver.out
stderr_logfile=/home/discordproxy/discordproxyserver.err
```

Add discordproxy to your supervisor configuration and restart supervisor to activate the change:

```bash
ln -s /home/discordproxy/discordproxyserver.conf /etc/supervisor/conf.d
supervisorctl reload
```

To verify your installation was successful we recommend to [test your server](#test-discord-proxy-server).

### Test Discord Proxy server

To verify your Discord Proxy server is up and running send a direct message to yourself with the CLI tool (replace the number below with your own user ID):

```bash
discordproxymessage direct 12345678 "test"
```

```{eval-rst}
.. hint::
    Here is how you can find IDs on your Discord server: `Where can I find my User/Server/Message ID? <https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID->`_
```

## Server configuration

Discord Proxy is designed to run via [supervisor](https://pypi.org/project/supervisor/) and can be configured with the below arguments. It comes with sensible defaults and will in most cases only need the Discord bot token to operate.

To configure your server just add/modify one of the below parameters in the respective command line of your supervisor configuration:

```text
usage: discordproxyserver [-h] [--token TOKEN] [--host HOST] [--port PORT]
                          [--log-console-level {DEBUG,INFO,WARN,ERROR,CRITICAL}]
                          [--log-file-level {DEBUG,INFO,WARN,ERROR,CRITICAL}]
                          [--log-file-path LOG_FILE_PATH] [--version]

Server with HTTP API for sending messages to Discord

optional arguments:
  -h, --help            show this help message and exit
  --token TOKEN         Discord bot token. Can alternatively be specified as
                        environment variable DISCORD_BOT_TOKEN. (default:
                        None)
  --host HOST           server host address (default: 127.0.0.1)
  --port PORT           server port (default: 50051)
  --log-console-level {DEBUG,INFO,WARN,ERROR,CRITICAL}
                        Log level of log file (default: CRITICAL)
  --log-file-level {DEBUG,INFO,WARN,ERROR,CRITICAL}
                        Log level of log file (default: INFO)
  --log-file-path LOG_FILE_PATH
                        Path for storing the log file. If no path if provided,
                        the log file will be stored in the current working
                        folder (default: None)
  --version             show the program version and exit
```

## Tools

Discord Proxy comes with a simple tool for sending messages to your Discord server from the command line. The main purpose of this tool is to check that the server is functioning correctly.

Here is how to send a direct message to a user:

```bash
discordproxymessage direct 12345678 "hi!"
```

The number is the user ID of the user to sent a message to. For more information run the command with the `-h` option.
