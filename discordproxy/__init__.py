"""Proxy server for accessing the Discord API via gRPC."""

__title__ = "Discord Proxy"
__version__ = "1.3.2"
